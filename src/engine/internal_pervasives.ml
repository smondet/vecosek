include Base
module Printf = Caml.Printf

module Time = struct
  let now () = Unix.gettimeofday ()
end

let dbg fmt = Printf.eprintf Caml.(">> Vecodbg: " ^^ fmt ^^ "\n%!")
let summarize s = try String.sub s ~pos:0 ~len:40 with _ -> s
