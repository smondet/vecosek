open Common
open Printf

module Instrument = struct
  type t = [ `Click | `Named of string ]
end

module Event = struct
  type t =
    | Tempo_operation of
        [ `Decr of int | `Incr of int | `Mul of float | `Set of int ]
    | Message_on of [ `Announcement | `Comment ] * string
    | Message_off of [ `Announcement | `Comment ]
    | Note of {
        instrument : Instrument.t;
        note : int;
        velocity : int;
        length : Q.t;
      }
    | Midi of {
        instrument : Instrument.t;
        status : int;
        channel : int;
        data_1 : int;
        data_2 : int;
      }
end

module Part = struct
  type ticked = { tick : Q.t; event : [ `Part of t | `Event of Event.t ] }
  and t = { length : Q.t; events : ticked list; name : string }

  type part = t

  let part name ~length events = { name; length; events }
  let ticked tick event = { tick; event }
  let event e = `Event e

  open Event

  let ticked_event e t = ticked t (event e)
  let bpm_incr t i = ticked t @@ event @@ Tempo_operation (`Incr i)
  let bpm_decr t i = ticked t @@ event @@ Tempo_operation (`Decr i)
  let bpm_set t i = ticked t @@ event @@ Tempo_operation (`Set i)
  let bpm_mul t i = ticked t @@ event @@ Tempo_operation (`Mul i)
  let play t p = ticked t (`Part p)
  let ann_on t msg = ticked_event (Message_on (`Announcement, msg)) t
  let ann_onf t fmt = Printf.ksprintf (ann_on t) fmt
  let ann_off t = ticked_event (Message_off `Announcement) t
  let comment_on t msg = ticked_event (Message_on (`Comment, msg)) t
  let comment_onf t fmt = Printf.ksprintf (ann_on t) fmt
  let comment_off t = ticked_event (Message_off `Comment) t

  let midi ~instrument ~status ~channel ~data_1 ~data_2 () =
    Midi { instrument; status; channel; data_1; data_2 }

  let note ~instrument ?(velocity = 100) note length =
    Note { instrument; note; velocity; length }

  module Relative = struct
    type t =
      | Note of { pitch : int; velocity : int option; length : Q.t }
      | Fork of t list list
      | Rest of Q.t

    let make_note ?velocity pitch length = Note { pitch; velocity; length }
    let make_rest length = Rest length
  end

  module RC = struct
    let n = Relative.make_note
    let r = Relative.make_rest
    let par l = Relative.Fork l
    let chord l dur = par (List.map l ~f:(fun p -> [ n p dur ]))

    let list ~instrument ~from l =
      let open Relative in
      let rec convert ~pos l =
        List.fold ~init:([], pos) l ~f:(fun (l, pos) -> function
          | Fork ll ->
              let parallel_voices = List.map ll ~f:(convert ~pos) in
              let max_pos =
                List.fold parallel_voices ~init:pos ~f:(fun p (_, posend) ->
                    Q.max p posend)
              in
              let events = List.concat_map parallel_voices ~f:fst in
              (* printf "pos: %f max pos: %s = %f, evs: %d\n%!" (Q.to_float pos) *)
              (*   (Q.to_string max_pos) (Q.to_float max_pos) *)
              (*   (List.length events) *)
              (* ; *)
              (events @ l, max_pos)
          | Note { pitch; velocity; length } ->
              (* printf "note-pos: %f\n%!" (Q.to_float pos); *)
              ( ticked_event
                  (note ~instrument ?velocity (from + pitch) length)
                  pos
                :: l,
                Q.(pos + length) )
          | Rest length -> (l, Q.(pos + length)))
      in
      convert ~pos:Q.one l |> fst
  end

  (** Merge 2 parts *)
  let merge pa pb =
    {
      length = Q.max pa.length pb.length;
      events = pa.events @ pb.events;
      name = Printf.sprintf "%s ++ %s" pa.name pb.name;
    }

  let append pa pb =
    {
      length = Q.( + ) pa.length pb.length;
      events =
        pa.events
        @ List.map pb.events ~f:(fun { tick; event } ->
              { tick = Q.( + ) tick pa.length; event });
      name = Printf.sprintf "%s then %s" pa.name pb.name;
    }

  let concat = function
    | [] -> failwith "Part.concat"
    | one :: more -> List.fold ~init:one more ~f:append

  let multiply n p =
    match n with
    | 0 -> Format.kasprintf failwith "multiply part by 0: %s" p.name
    | _ -> List.init n ~f:(fun _ -> p) |> concat

  let filter_out how p =
    { p with events = List.filter p.events ~f:(fun e -> not (how e)) }

  let event_after q { tick; _ } = Float.(Q.to_float tick > Q.to_float q)

  let trim ~length p =
    let pp = filter_out (event_after Q.(length + (19 / 20))) p in
    { pp with length }

  let ( !! ) n evs = part ~length:Q.(i n) "Anonymous part" evs

  module Midi_file = struct
    type track = {
      full_name : string;
      title : string;
      length : Q.t;
      events : (Instrument.t -> ticked) list;
    }

    type t = { parsed : Stamifi.Midi.File.t; tracks : track list }

    let of_file_path path =
      let open Stamifi in
      let (Midi.File.
             {
               id = _;
               header_length = _;
               midi_format = _;
               track_number = _;
               per_quarter_note;
               tracks;
             } as parsed) =
        Midi_file.parse_smf path
      in
      let make_part idx track =
        let name = Printf.sprintf "%s.(%d)" path idx in
        let events, end_of_track, title_opt =
          let open Midi in
          let midi_events = ref [] in
          let title = ref None in
          let end_of_track = ref None in
          let cur_tick = ref 0 in
          Array.iter track.Track.events ~f:(function
            | Event.Empty -> ()
            | Event.Midi { ticks; status; channel; data_1; data_2 } ->
                cur_tick := !cur_tick + ticks;
                let tick = Q.(i 1 + (!cur_tick / per_quarter_note)) in
                midi_events :=
                  (fun instrument ->
                    ticked_event
                      (midi ~instrument ~status ~channel ~data_1 ~data_2 ())
                      tick)
                  :: !midi_events
            | Midi.Event.Meta { Midi.Meta_event.ticks; service_id = 0x2f; _ } ->
                cur_tick := !cur_tick + ticks;
                end_of_track := Some !cur_tick
            | Midi.Event.Meta
                { Midi.Meta_event.ticks; service_id = 0x03; service_data } ->
                cur_tick := !cur_tick + ticks;
                title :=
                  Some
                    (Array.to_list service_data
                    |> List.map ~f:Char.of_int_exn
                    |> String.of_char_list)
            | Event.Meta { ticks; _ } -> cur_tick := !cur_tick + ticks
            | Event.Sys _ -> ());
          (* printf "Parsing %S: title: %S\n%!" *)
          (*   name *)
          (*   (Option.value ~default:"NONE" !title); *)
          ( List.rev !midi_events,
            Option.value_exn
              ~message:(Printf.sprintf "%s has no end marker" name)
              !end_of_track,
            !title )
        in
        let length = Q.(end_of_track / per_quarter_note) in
        let title = Option.value ~default:"UNTITLED" title_opt in
        let full_name = Printf.sprintf "%s: %S" name title in
        { full_name; title; length; events }
      in
      { parsed; tracks = Array.to_list tracks |> List.mapi ~f:make_part }

    let get_part_exn { tracks; _ } ~instrument how =
      match how with
      | `Index n ->
          Option.(
            List.nth tracks n >>= fun { title; length; events; _ } ->
            return
              (part title ~length
                 (List.map events ~f:(fun ev -> ev instrument))))
          |> Option.value_exn ~message:(Printf.sprintf "Can't find part %d" n)
      | `Title_is f ->
          Option.(
            List.find ~f:(fun { title; _ } -> f title) tracks
            >>= fun { title; length; events; _ } ->
            (* printf "Got part: %S Q.(%s)\n%!" *)
            (*   title Q.(to_string length); *)
            return
              (part title ~length
                 (List.map events ~f:(fun ev -> ev instrument))))
          |> Option.value_exn
               ~message:
                 (Printf.sprintf "Can't find part matching criteria in [%s]"
                    (String.concat ~sep:", "
                       (List.map tracks ~f:(fun { title; _ } -> title))))
  end

  type of_instrument = Instrument.t -> part

  let of_code_generator ~location midi_track =
    (* dbg "processing: %s" midi_track#name ; *)
    let events, end_of_track, _ =
      let midi_events = ref [] in
      let title = ref None in
      let end_of_track = ref None in
      let cur_tick = ref 0 in
      let fix_ticks ticks =
        (* We need to translate the ones that do not start at 0. *)
        match midi_track#start with
        | None -> Some ticks
        | Some f ->
            let fixed =
              ticks - Float.(f * of_int midi_track#per_quarter_note |> to_int)
            in
            if fixed < 0 || (fixed = 0 && ticks <> 0) || fixed = 1 then
              dbg "%s fixed ticks %s: %d -> %d (start: %.2f)\n%!" location
                midi_track#name ticks fixed f;
            if fixed >= 0 then Some fixed else None
      in
      let process_event :
          [ `Meta of int * int * int array
          | `Midi of int * int * int * int * int ] ->
          unit = function
        | `Midi (ticks, status, channel, data_1, data_2) ->
            cur_tick := !cur_tick + ticks;
            Option.iter (fix_ticks !cur_tick) ~f:(fun tk ->
                let tick = Q.(i 1 + (tk / midi_track#per_quarter_note)) in
                midi_events :=
                  (fun instrument ->
                    ticked_event
                      (midi ~instrument ~status ~channel ~data_1 ~data_2 ())
                      tick)
                  :: !midi_events)
        | `Meta (ticks, 0x2f, _) ->
            cur_tick := !cur_tick + ticks;
            end_of_track := fix_ticks !cur_tick
        | `Meta (ticks, 0x03, service_data) ->
            cur_tick := !cur_tick + ticks;
            title :=
              Some
                (Array.to_list service_data
                |> List.map ~f:Char.of_int_exn
                |> String.of_char_list)
        | `Meta (ticks, _, _) -> cur_tick := !cur_tick + ticks
      in
      Array.iter midi_track#events ~f:process_event;
      ( List.rev !midi_events,
        Option.value_exn
          ~message:
            (Printf.sprintf "%s/%s has no end marker" location midi_track#name)
          !end_of_track,
        !title )
    in
    let length =
      match midi_track#length with
      | None -> Q.(end_of_track / midi_track#per_quarter_note)
      | Some l -> Q.(Float.(iround_exn (l * 1_000.)) / 1_000)
    in
    let title = sprintf "%s/%s" location midi_track#name in
    object (self)
      method part_of_instrument : of_instrument =
        fun instrument ->
          part title ~length (List.map events ~f:(fun ev -> ev instrument))

      method part =
        self#part_of_instrument
          (`Named
            (Option.value_exn midi_track#instrument
               ~message:
                 (Printf.sprintf "MIDI-Track %s/%s does not have an instrument"
                    location midi_track#name)))
    end
end

module Chunk = struct
  type time_signature =
    | TS_3_4
    | TS_4_4
    | TS_8_4
    | TS_9_4
    | TS_5_4
    | TS_6_4
    | TS_10_4
    | TS_11_4
    | TS_14_4
    | TS_7_4
    | TS_12_8
    | TS_33_8
    | TS_36_8

  type multiplier = [ `Once | `Infinity ]

  type meta_handler = {
    on : string;  (** The event id in the [custom_buttons] list. *)
    one_shot : bool;
    actions : [ `Raw of Vecosek_scene.Scene.Action.t ] list;
  }

  type t =
    | Set_tempo of int
    | Moment of { count_in_tempo : int; title : string }
    | Time_signature of time_signature
    | Part_full of {
        part_title : string;
        next : (Q.t * string) option;
        multiply : multiplier;
        part : Part.t;
      }
    | List of t list
    | Set_handlers of meta_handler list

  let duration ?(current_tempo = 120.) ?(ppqn = 192) chunk =
    let rec find_tempo_ops at_tick events =
      let open Part in
      List.find_map events ~f:(function
        | { tick; event = `Part { events; _ } } ->
            find_tempo_ops (at_tick + Q.to_ticks ~ppqn tick - ppqn) events
        | { tick; event = `Event (Event.Tempo_operation op) }
          when Q.to_ticks ~ppqn tick - ppqn = at_tick ->
            Some op
        | { tick = _; event = `Event _ } -> None)
    in
    let bpm = ref current_tempo in
    let time = ref 0. in
    let rec go_deep = function
      | Part_full { part = { Part.length; events; _ }; _ } ->
          for i = 0 to Q.to_ticks ~ppqn length do
            (match find_tempo_ops i events with
            | Some (`Incr i) -> bpm := !bpm +. Float.of_int i
            | Some (`Decr i) -> bpm := !bpm -. Float.of_int i
            | Some (`Mul m) -> bpm := !bpm *. m
            | Some (`Set s) -> bpm := Float.of_int s
            | None -> ());
            time := !time +. (60. /. (!bpm *. Float.of_int ppqn))
          done
      | List l -> List.iter l ~f:go_deep
      | Set_tempo t -> bpm := Float.of_int t
      | Moment _ | Time_signature _ | Set_handlers _ -> ()
    in
    go_deep chunk;
    !time

  let rec to_markdown =
    let time_signature = function
      | TS_3_4 -> "3/4"
      | TS_4_4 -> "4/4"
      | TS_8_4 -> "8/4"
      | TS_9_4 -> "9/4"
      | TS_5_4 -> "5/4"
      | TS_6_4 -> "6/4"
      | TS_10_4 -> "10/4"
      | TS_11_4 -> "11/4"
      | TS_14_4 -> "14/4"
      | TS_7_4 -> "7/4"
      | TS_12_8 -> "12/8"
      | TS_33_8 -> "33/8"
      | TS_36_8 -> "36/8"
    in
    let multiplier = function `Once -> "" | `Infinity -> "**∞** × " in
    function
    | Set_tempo i -> sprintf "- tempo <- %d" i
    | Moment { count_in_tempo; title } ->
        sprintf "\n**%s (%dBPM)**\n" title count_in_tempo
    | Time_signature ts -> sprintf "\nTS *%s*\n" (time_signature ts)
    | Part_full { part_title; next; multiply; part } ->
        sprintf "- %s%s (part: %s%s)" (multiplier multiply) part_title
          part.Part.name
          (Option.value_map ~default:"" next ~f:(fun (_q, s) ->
               sprintf ", next: %s" s))
    | List l -> List.map ~f:to_markdown l |> String.concat ~sep:"\n"
    | Set_handlers l ->
        sprintf "- set-handlers: %s"
          (String.concat ~sep:", " (List.map l ~f:(fun x -> x.on)))

  let first_moment_title chunk =
    let rec find = function
      | Set_handlers _ | Time_signature _ | Part_full _ | Set_tempo _ -> None
      | Moment { count_in_tempo = _; title } -> Some title
      | List l -> List.find_map l ~f:find
    in
    find chunk

  let to_summary chunk =
    let current_moment = ref None in
    let infinities = ref [] in
    let parts = ref [] in
    let summary = ref [] in
    let rec go = function
      | Set_handlers _ | Time_signature _ -> ()
      | Set_tempo _ as s -> parts := s :: !parts
      | Moment { count_in_tempo; title } ->
          summary :=
            (!current_moment, List.rev !infinities, List.rev !parts) :: !summary;
          infinities := [];
          parts := [];
          current_moment := Some (count_in_tempo, title)
          (* sprintf "\n**%s (%dBPM)**\n" title count_in_tmepo *)
      | Part_full { part_title; next; multiply; part } as p ->
          (match multiply with
          | `Infinity ->
              infinities :=
                `Infinity_full (part_title, next, part) :: !infinities
          | `Once -> ());
          parts := p :: !parts (* TODO *)
      | List l ->
          List.iter l ~f:go;
          ()
    in
    go chunk;
    summary := (!current_moment, !infinities, !parts) :: !summary;
    List.rev !summary
end

open Chunk

let moment ~bpm s = Moment { count_in_tempo = bpm; title = s }
let tempo i = Set_tempo i
let list l = List l
let times x = `Times x
let infinity = `Infinity

type t = Chunk.t list

let p ?next ?(next_before = Q.(i 4)) multiply part_title part =
  Part_full
    {
      multiply;
      part_title;
      part;
      next = Option.map next ~f:(fun n -> (next_before, n));
    }

let infinite ?next ?next_before = p ?next ?next_before `Infinity
let one ?next ?next_before = p ?next ?next_before `Once

(* let two ?next ?next_before = p ?next ?next_before (`Times 4) *)
(* let four ?next ?next_before = p ?next ?next_before (`Times 4) *)

let list_f n f = list (List.init n ~f:(fun i -> f (i + 1)))

let list_times n f =
  list
    (List.init n ~f:(fun i ->
         f ~progress:(sprintf "[%d/%d]" (i + 1) n) ~nth:(i + 1)))

let bpm_incr i = `Raw (Vecosek_scene.Scene.Action.Bpm_operation (`Incr i))
let bpm_decr i = `Raw (Vecosek_scene.Scene.Action.Bpm_operation (`Decr i))
let handler ?(one_shot = false) ~on actions = { on; one_shot; actions }
let set_handlers l = Set_handlers l

let one_times_f ?next_before ?next n msg part_f =
  list_times n (fun ~progress ~nth ->
      one
        (sprintf "%s %s" msg progress)
        (part_f nth) ?next_before
        ?next:(if nth = n then next else None))

let one_times ?next_before ?next n msg part =
  one_times_f ?next_before ?next n msg (fun _ -> part)

module Button = struct
  type t = {
    name : string;
    event : Vecosek_scene.Scene.Event.t;
        (* should be just vecosek-free -events? *)
  }

  let make ~name ~event = { name; event }
  let name t = t.name
  let event t = t.event
end

let to_markdown l = Chunk.to_markdown (List l)

module Summary = struct
  let time_to_string time_length =
    sprintf "%02d:%02d.%03d"
      (Int.of_float (time_length /. 60.))
      (Int.of_float time_length % 60)
      (Int.of_float (time_length *. 1000.) - Int.of_float time_length)

  let to_markdown l =
    dbg "Computing summary";
    let s = Chunk.to_summary (List l) in
    let b = Buffer.create 42 in
    dbg "Getting total length";
    let time_length = Chunk.duration (List l) in
    let outf fmt = ksprintf (Buffer.add_string b) fmt in
    let running_time = ref 0. in
    outf "Total length: `%s`\n" (time_to_string time_length);
    (* (int_of_float (time_length /. 60.)) *)
    (* (int_of_float time_length mod 60) *)
    (* (int_of_float (time_length *. 1000.) - int_of_float time_length) ; *)
    dbg "Building summary markdown";
    let all_titles = ref [] in
    List.iter s ~f:(fun (moment_opt, infinities, parts) ->
        (match moment_opt with
        | None -> outf "- **No Moment**\n"
        | Some (bpm, title) ->
            all_titles := title :: !all_titles;
            let ltime =
              Chunk.duration ~current_tempo:(Float.of_int bpm) (List parts)
            in
            outf "- `%s`: **%s** (%d bpm) → `%s`\n"
              (time_to_string !running_time)
              title bpm (time_to_string ltime);
            running_time := !running_time +. ltime);
        List.iter infinities ~f:(function
          | `Infinity { Part.length; name; _ } ->
              outf "    - ∞ %s (length: %s)\n" name (Q.to_string length)
          | `Infinity_full (titl, nxt, { Part.length; _ }) ->
              outf "    - ∞ %s (length: %s)%s\n" titl (Q.to_string length)
                (Option.value_map nxt ~default:"" ~f:(fun (_, n) ->
                     sprintf " (next: %s)" n)));
        ());
    dbg "last running time: %s" (time_to_string !running_time);
    let mmts = List.length !all_titles in
    let dups =
      List.dedup_and_sort !all_titles ~compare:String.compare |> List.length
    in
    outf "\n%d moments." mmts;
    if mmts <> dups then
      outf "WARNING: There are duplicate moments, e.g. %S"
        (List.find_a_dup !all_titles ~compare:String.compare
        |> Option.value_exn ~message:"did not find a dup");
    Buffer.contents b

  let to_html_micro_format ?name l =
    dbg "Computing summary";
    let s = Chunk.to_summary (List l) in
    let b = Buffer.create 42 in
    dbg "Getting total length";
    let time_length = Chunk.duration (List l) in
    let last_is_nl = ref true in
    let outf fmt =
      ksprintf
        (fun s ->
          Buffer.add_string b s;
          last_is_nl := false)
        fmt
    in
    let o ?(a = []) tag =
      outf "<%s%s>" tag
        (String.concat ~sep:""
           (List.map ~f:(fun (k, v) -> sprintf " %s=%S" k v) a))
    in
    let c tag = outf "</%s>" tag in
    let indentation = ref 0 in
    let base_indent = 2 in
    let indent () = indentation := !indentation + base_indent in
    let deindent () = indentation := !indentation - base_indent in
    let nl () =
      if !last_is_nl then ()
      else (
        outf "\n%s" (String.make !indentation ' ');
        last_is_nl := true)
    in
    let t tag ?a f =
      o ?a tag;
      f ();
      c tag
    in
    let tn tag ?a f =
      nl ();
      o ?a tag;
      indent ();
      nl ();
      f ();
      deindent ();
      nl ();
      c tag
    in
    let pcf fmt = ksprintf (fun s () -> outf "%s" s) fmt in
    let running_time = ref 0. in
    let cl n = ("class", n) in
    let div = tn "div" in
    let divclass s = div ~a:[ cl s ] in
    divclass "mix-summary" (fun () ->
        divclass "global" (fun () ->
            Option.iter name ~f:(fun n ->
                t "span" ~a:[ cl "name" ] (pcf "%s" n);
                nl ());
            t "code"
              ~a:[ cl "total-length" ]
              (pcf "%s" (time_to_string time_length));
            ());
        tn "ol"
          ~a:[ cl "moments" ]
          (fun () ->
            List.iteri s ~f:(fun ith (moment_opt, infinities, parts) ->
                (match moment_opt with
                | None -> dbg "No Moment (%d)" ith
                | Some (bpm, title) ->
                    let ltime =
                      Chunk.duration ~current_tempo:(Float.of_int bpm)
                        (List parts)
                    in
                    tn "li"
                      ~a:[ cl "moment" ]
                      (fun () ->
                        t "code"
                          ~a:[ cl "start" ]
                          (pcf "%s" (time_to_string !running_time));
                        nl ();
                        t "span" ~a:[ cl "title" ] (pcf "%s" title);
                        nl ();
                        t "code"
                          ~a:[ cl "length" ]
                          (pcf "%s" (time_to_string ltime));
                        nl ();
                        t "code" ~a:[ cl "start-bpm" ] (pcf "%d" bpm);
                        ());
                    running_time := !running_time +. ltime);
                List.iter infinities ~f:(function
                  | `Infinity { Part.length; name; _ } ->
                      dbg "    - ∞ %s (length: %s)\n" name (Q.to_string length)
                  | `Infinity_full (titl, nxt, { Part.length; _ }) ->
                      dbg "    - ∞ %s (length: %s)%s\n" titl
                        (Q.to_string length)
                        (Option.value_map nxt ~default:"" ~f:(fun (_, n) ->
                             sprintf " (next: %s)" n)));
                ());
            ());
        ());
    Buffer.contents b
end

let rec flatten l =
  let res = ref [] in
  let rec loop = function
    | [] -> ()
    | List h :: l ->
        res := !res @ flatten h;
        loop l
    | other :: l ->
        res := !res @ [ other ];
        loop l
  in
  loop l;
  !res

let filter_from ~prefix l =
  List.drop_while (flatten l)
    ~f:
      Chunk.(
        function
        | Moment { title; _ } when String.is_prefix title ~prefix -> false
        | List _l -> assert false (* We double-check that `flatten` worked :) *)
        | Moment { title; _ } ->
            printf "PREFIX: of %S (%S)\n" title prefix;
            true
        | _ -> true)

type global_parameters = {
  if_live : Chunk.t list -> Chunk.t list -> Chunk.t;
  live : bool;
  testing : bool;
}

let make_global_parameters ~live ~testing : global_parameters =
  { live; if_live = (fun t f -> if live then list t else list f); testing }

module C = struct
  open Part
  (* module P = struct type t = < part: part > end open P *)

  let inj (* : part -> < part : part; .. > *) (p : part) =
    object
      method part = p
    end

  let ( !: ) p = inj p#part
  let multiply n pa = inj (multiply n pa#part)
  let ( ** ) pa n = multiply n pa
  let random (l : < part : part ; .. > list) = List.random_element_exn l ** 1
  let ( // ) pa pb = inj (merge pa#part pb#part)
  let ( %% ) pa pb = concat [ pa#part; pb#part ] |> inj
  let part ~length title evs = inj (part ~length title evs)
  let silence_q n = part ~length:Q.(i n) (Printf.sprintf "silence %d q" n) []
  let chunk c : Chunk.t = c

  let pp ppf part =
    let open Caml.Format in
    let p : part = part#part in
    let open Part in
    fprintf ppf "(part %s, length: %s = %.02f, events: %d)" p.name
      (Q.to_string p.length) (Q.to_float p.length) (List.length p.events)

  module Filter = struct
    let out how p =
      inj
        {
          (p#part) with
          events = List.filter p#part.events ~f:(fun e -> not (how e));
        }

    let event_after q { tick; _ } = Float.(Q.to_float tick > Q.to_float q)

    let trim ~length p =
      let pp = filter_out (event_after Q.(length + (19 / 20))) p#part in
      inj { pp with length }
  end

  module Chunk = struct
    let f ?next_before ?next n msg part_f =
      one_times_f ?next_before ?next n msg (fun nth -> (part_f nth)#part)

    let once ?next_before ?next msg p = one ?next_before ?next msg p#part

    let infinite ?next_before ?next msg p =
      infinite ?next_before ?next msg p#part

    let list = list
    let moment = moment
    let tempo = tempo
    let ts_3_4 = Time_signature TS_3_4
    let ts_4_4 = Time_signature TS_4_4
    let ts_8_4 = Time_signature TS_8_4
    let ts_9_4 = Time_signature TS_9_4
    let ts_5_4 = Time_signature TS_5_4
    let ts_6_4 = Time_signature TS_6_4
    let ts_10_4 = Time_signature TS_10_4
    let ts_11_4 = Time_signature TS_11_4
    let ts_14_4 = Time_signature TS_14_4
    let ts_7_4 = Time_signature TS_7_4
    let ts_12_8 = Time_signature TS_12_8
    let ts_33_8 = Time_signature TS_33_8
    let ts_36_8 = Time_signature TS_36_8
  end
end

module Edit = struct
  let change_events ~f ~title ~next ~multiply (p : Part.t) =
    let p =
      let open Part in
      let rec part { length; events; name } =
        { length; name; events = List.map ~f:ticked events }
      and ticked { tick; event } =
        {
          tick;
          event =
            (match event with
            | `Event ev -> `Event (f ev)
            | `Part p -> `Part (part p));
        }
      in
      part p
    in
    Part_full { part_title = title; next; multiply; part = p }

  let rec map_parts (chunk : Chunk.t) ~f =
    match chunk with
    | Set_tempo _ | Moment _ | Time_signature _ | Set_handlers _ -> chunk
    | Part_full { part_title; next; multiply; part } ->
        f ~title:part_title ~next ~multiply part
    | List l -> List (List.map l ~f:(map_parts ~f))
end
