open Common
open Mix
open Printf

module type Compilation_options = sig
  val bpm : int
  val ppqn : int
  val next_buttons : Button.t list
  val prev_moment_buttons : Button.t list
  val next_moment_buttons : Button.t list
  val incr_bpm_buttons : Button.t list
  val decr_bpm_buttons : Button.t list
  val custom_buttons : (string * Button.t list) list
  val vidimetro_port : int
  val metronome_with_sound : [ `On_click ] option
  val drums_port : int
  val instruments_mapping : (Mix.Instrument.t * int * int) list
  val click_note_one : unit -> int
  val click_note_other : unit -> int
end

module Default_options = struct
  let decr_bpm_buttons = []
  let incr_bpm_buttons = []
  let next_moment_buttons = []
  let prev_moment_buttons = []
  let next_buttons = []
  let custom_buttons = []
  let vidimetro_port = 3
  let bpm = 120
  let ppqn = 192
  let metronome_with_sound = Some `On_click
  let drums_port = 0

  let instruments_mapping =
    [
      (`Named "bass-synth", 2, 0);
      (`Named "bass", 2, 1);
      (`Named "melody", 2, 2);
      (`Named "guitar", 2, 3);
      (`Named "sampler-drums", 0, 10);
      (`Named "sampler-cymbals", 1, 10);
      (`Named "sampler-samples", 4, 0);
      (`Click, 0, 10);
    ]
end

module Make (Parameters : Compilation_options) = struct
  open Parameters
  open Vecosek_scene
  open Chunk

  module Internal = struct
    let note_on = 0x90
    let note_off = 0x80

    let note_on_off ~port ~tick ~length ~channel ~pitch ~velocity =
      Scene.
        [
          Ticked_action.make ~tick
            ~action:
              (Action.Raw_midi
                 (Midi_event.make ~port ~status:note_on ~channel ~data1:pitch
                    ~data2:velocity ()));
          Ticked_action.make ~tick:(tick + length)
            ~action:
              (Action.Raw_midi
                 (Midi_event.make ~port ~status:note_off ~channel ~data1:pitch
                    ()));
        ]

    let check name cond =
      if not cond then (
        eprintf "Condition failed: %s\n%!" name;
        ksprintf failwith "Condition failed: %s" name)

    let length_ticks d =
      check "length: ≥ 0" Float.(Q.to_float d >= 0.);
      Q.to_ticks ~ppqn d

    let position_ticks p =
      let f = Q.to_float p in
      let ticks = Q.to_ticks ~ppqn Q.(p - i 1) in
      check
        (sprintf "position should be %f ≥ 1 (%s = %d ticks)" f (Q.to_string p)
           ticks)
        Float.(f >= 1.);
      ticks
  end

  open Internal

  let ticked pos action =
    let tpos =
      try position_ticks pos
      with e ->
        Printf.eprintf "ticked: (%s, %s)\n%!" (Q.to_string pos)
          (Vecosek_scene.Scene_format_j.string_of_action action);
        raise e
    in
    Scene.Ticked_action.make ~tick:tpos ~action

  let q_epsilon = Q.(1 / ppqn)
  let q_first = Q.(i 1 + q_epsilon)
  let q_second = Q.(i 1 + (2 / ppqn))

  module Vidimetro = struct
    let action ~status ~chan ~data1 ~data2 =
      Scene.Action.(
        Raw_midi
          (Scene.Midi_event.make ~port:vidimetro_port ~status ~channel:chan
             ~data1 ~data2 ()))

    let act ~status ?(chan = 0) ~data1 ?(data2 = 0) pos =
      ticked pos (action ~status ~chan ~data1 ~data2)

    let write_line ~line ~text =
      let msg =
        if String.length text > 63 then String.sub text ~pos:0 ~len:63 ^ "#"
        else text
      in
      let status = 0x80 in
      let encode_byte index c =
        let code = Char.to_int c in
        (* let chan = k in *)
        let data1 =
          0b0111_1111
          land ((line lsl 3) land 0b0111_1000
               lor ((index land 0b0011_1000) lsr 3))
        in
        let data2 lr =
          ((index land 0b0111) lsl 4)
          lor
          match lr with
          | `Left -> (code land 0b1111_0000) lsr 4
          | `Right -> code land 0b1111
        in
        [
          action ~status ~chan:6 ~data1 ~data2:(data2 `Left);
          action ~status ~chan:7 ~data1 ~data2:(data2 `Right);
        ]
      in
      String.to_list msg |> List.concat_mapi ~f:encode_byte

    let empty_line l = action ~status:0x80 ~data1:0 ~chan:5 ~data2:l

    let set_line ~position ~line ~text =
      (*
      let msg =
        if String.length text > 63
        then String.sub_exn text ~index:0 ~length:62 ^ "#"
        else text in
      let status = 0x80 in
      let encode_byte pos index c =
        let code = int_of_char c in
        (* let chan = k in *)
        let data1 =
          0b0111_1111 land (
            ((line lsl 3) land 0b0111_1000)
            lor ((index land 0b0011_1000) lsr 3)
          )
        in
        let data2 lr =
          ((index land 0b0111) lsl 4) lor
          (match lr with
          | `Left -> (code land 0b1111_0000) lsr 4
          | `Right -> (code land 0b1111)) in
        [
          act pos ~status ~chan:6 ~data1 ~data2:(data2 `Left);
          act pos ~status ~chan:7 ~data1 ~data2:(data2 `Right);
        ]
      in
         *)
      [ ticked position (empty_line line) (* Erase line *) ]
      @ List.map ~f:(ticked Q.(position + (1 / 100))) (write_line ~line ~text)

    (*   String.to_character_list msg *)
    (* |> List.concat_mapi ~f:(encode_byte Q.(position + 1/100))) *)

    let set_bar_structure pos ~length ~bar_structure =
      let status = 0x80 in
      let chan = 4 in
      [
        act pos (* bar structure length *) ~status ~chan ~data1:0 ~data2:length;
        act pos (* bar structure reset *) ~status ~chan ~data1:2 ~data2:0;
      ]
      @ (List.mapi bar_structure ~f:(fun index -> function
           | `Strong ->
               Some (act Q.(pos + (1 / 60)) ~status ~chan ~data1:1 ~data2:index)
           | `Weak -> None)
        |> List.filter_opt)

    let beats_and_progress ~length =
      let status = 0x80 in
      List.init length ~f:(fun beat ->
          let subs = 20 in
          [
            act
              Q.(i beat + i 1) (* beat on *)
              ~status ~chan:2 ~data1:0 ~data2:beat;
            act
              Q.(i beat + i 1 + q 1 6) (* beat off *)
              ~status ~chan:2 ~data1:1 ~data2:0;
          ]
          @ (List.init subs ~f:(fun sub ->
                 [
                   (* Beat Progress: *)
                   act
                     Q.(i beat + i 1 + q sub subs)
                     ~status ~chan:0
                     ~data1:
                       (let prog = 127 * sub / (subs - 1) in
                        if beat % 2 = 0 then prog else 127 - prog)
                     ~data2:0;
                   (* Bar Progress: *)
                   act
                     Q.(i beat + i 1 + q sub subs)
                     ~status ~chan:1 ~data1:beat
                     ~data2:(127 * sub / (subs - 1));
                 ])
            |> List.concat))
      |> List.concat
  end

  module Message = struct
    let add position (line, text) =
      try Vidimetro.set_line ~position ~line ~text
      with e ->
        dbg "Vidimetro.set_line: %s, %d, %s" (Q.to_string position) line text;
        raise e

    let display_for _l (_k, _msg) = []
    let moment_title_key = 0
    let part_title_key = 1
    let next_announcement_key = 3
    let announcement_key = 2
    let comment_key = 6
    let bpm_key = 5

    (* let next_key = 6 *)

    let set_moment_title title = add q_second (moment_title_key, title)
    let set_part_title title = add q_second (part_title_key, "## " ^ title)

    let set_next_announcement tick msg =
      add tick (next_announcement_key, "Next: " ^ msg)

    let set_announcement tick msg = add tick (announcement_key, msg)
    let unset_announcement tick = add tick (announcement_key, "—")
    let set_comment tick msg = add tick (comment_key, msg)
    let unset_comment tick = add tick (comment_key, "—")

    let set_next_announcement_action fmt =
      ksprintf
        (fun text ->
          let text = "Next: " ^ text in
          Vidimetro.write_line ~line:next_announcement_key ~text)
        fmt

    (* let set_next fmt = *)
    (*   ksprintf (fun text -> *)
    (*       Vidimetro.write_line ~line:next_key ~text *)
    (*     ) fmt *)

    (* let unset_next () = *)
    (*   Vidimetro.empty_line next_key *)
  end

  let bpm_op_to_string = function
    | `Incr i -> sprintf "BPM <- BPM + %d" i
    | `Decr i -> sprintf "BPM <- BPM - %d" i
    | `Set i -> sprintf "BPM <- %d" i
    | `Mul i -> sprintf "BPM <- BPM × %.3f" i

  let bpm_operation_actions op =
    Message.display_for
      (length_ticks Q.(i 4))
      (Message.bpm_key, bpm_op_to_string op)
    @ [ Scene.Action.Bpm_operation op ]

  let bpm_operation ?(tick = q_first) op =
    try bpm_operation_actions op |> List.map ~f:(ticked tick)
    with e ->
      dbg "bpm_operation raised tick: %s for %s" (Q.to_string tick)
        (bpm_op_to_string op);
      raise e

  let set_bpm t = bpm_operation (`Set t)
  let track_on pos id = [ ticked pos (Scene.Action.Track_on (id, 1)) ]
  let track_off pos id = [ ticked pos (Scene.Action.Track_off id) ]

  let add_handler pos name events actions =
    [ ticked pos (Scene.Action.Add_event_handler { name; events; actions }) ]

  let remove_handler_for_events pos events =
    List.map events ~f:(fun e ->
        ticked pos (Scene.Action.Remove_event_handler_by_event e))

  let handler_name buttons fmt =
    ksprintf
      (sprintf "{%s} → %s"
         (List.map buttons ~f:Button.name |> String.concat ~sep:"|"))
      fmt

  let set_next_handler ?(name_tag = "Next") ?(pos = q_second) ~end_of actions =
    let evs = List.map next_buttons ~f:Button.event in
    let name = handler_name next_buttons "Trigger %s" name_tag in
    let remove_all_next =
      List.map evs ~f:(fun e -> Scene.Action.Remove_event_handler_by_event e)
    in
    (* (Scene.Event.Track_ends end_of)) in *)
    remove_handler_for_events pos evs
    @ add_handler
        Q.(pos + q_epsilon)
        name evs
        (remove_all_next
        (* @ Message.set_next "%s ends → %s" end_of name_tag *)
        @ Message.set_next_announcement_action "%s" name_tag
        @ [
            Scene.Action.(
              Add_event_handler
                {
                  name = sprintf "(Next: end of %s)" end_of;
                  events = [ Scene.Event.Track_ends end_of ];
                  actions =
                    actions
                    @ [
                        (* Message.unset_next (); *)
                        Scene.Action.Remove_event_handler_by_event
                          (Scene.Event.Track_ends end_of);
                      ];
                });
          ])

  let set_moment_switch_handler ?(pos = q_second) ~buttons moment_id =
    let evs = List.map buttons ~f:Button.event in
    let name = handler_name buttons "Jump to %s" moment_id in
    let remove_all_for_buttons =
      List.map evs ~f:(fun e -> Scene.Action.Remove_event_handler_by_event e)
    in
    remove_handler_for_events pos evs
    @ add_handler
        Q.(pos + q_epsilon)
        name evs
        (remove_all_for_buttons
        @ Scene.Action.[ All_tracks_off; Track_on (moment_id, 1) ])

  module Main_track_id = struct
    type t = Id of int

    let first = Id 1
    let previous (Id i) = Id (i - 1)
    let next (Id i) = Id (i + 1)
    let render (Id i) = sprintf "main_track_%d" i
    let as_int (Id i) = i
  end

  let compile_meta_handler ?(pos = q_second) { on; actions; one_shot } =
    let buttons =
      List.Assoc.find custom_buttons on ~equal:Poly.equal
      |> Option.value_exn ~message:(sprintf "custom button not found: %S" on)
    in
    let evs = List.map buttons ~f:Button.event in
    let name = handler_name buttons "Custom %s" on in
    let remove_all_for_buttons =
      if one_shot then
        List.map evs ~f:(fun e -> Scene.Action.Remove_event_handler_by_event e)
      else []
    in
    let actions = List.map actions ~f:(function `Raw veco -> veco) in
    remove_handler_for_events pos evs
    @ add_handler Q.(pos + q_epsilon) name evs (remove_all_for_buttons @ actions)

  let find_instrument instruments_mapping instrument =
    match
      List.find_map instruments_mapping ~f:(fun (i, p, c) ->
          if Poly.equal i instrument then Some (p, c) else None)
    with
    | Some s -> s
    | None ->
        Format.kasprintf failwith "Instrument mapping not found: %S"
          (match instrument with `Click -> "THE-CLICK" | `Named name -> name)

  let rec compile_part_events ?(translate = Q.zero) (events : Part.ticked list)
      =
    let open Part in
    List.concat_map events ~f:(fun { tick; event } ->
        (* let tick_int = position_ticks tick in *)
        let tick = Q.(tick + translate) in
        match event with
        | `Part { events; _ } ->
            compile_part_events ~translate:Q.(tick - i 1) events
        | `Event e -> (
            let open Event in
            match e with
            | Tempo_operation op -> bpm_operation ~tick op
            | Message_on (`Announcement, msg) ->
                Message.set_announcement tick msg
            | Message_off `Announcement -> Message.unset_announcement tick
            | Message_on (`Comment, msg) -> Message.set_comment tick msg
            | Message_off `Comment -> Message.unset_comment tick
            | Note { instrument; note; velocity; length } ->
                (* let tpos = position_ticks tick in *)
                let tpos =
                  try position_ticks tick
                  with e ->
                    Printf.eprintf "Note: (note: %d, vel: %d)\n%!" note velocity;
                    raise e
                in
                let tlen =
                  (* Cheap hack: We want the last not to be always inside the track. *)
                  length_ticks length - 3
                in
                let port, channel =
                  find_instrument instruments_mapping instrument
                in
                note_on_off ~port ~velocity ~channel ~pitch:note ~tick:tpos
                  ~length:tlen
            | Midi { instrument; status; channel = _; data_1; data_2 } ->
                let port, channel =
                  find_instrument instruments_mapping instrument
                in
                let tpos =
                  try position_ticks tick
                  with e ->
                    Printf.eprintf "Midi: (port: %d, status: %d)\n%!" port
                      status;
                    raise e
                in
                let open Vecosek_scene.Scene in
                [
                  Ticked_action.make ~tick:tpos
                    ~action:
                      (Action.Raw_midi
                         (Midi_event.make ~port ~status ~channel ~data1:data_1
                            ~data2:data_2 ()));
                ]))

  let compile_part ?previous_moment_id ~handlers ?next_moment_id
      ?next_announcement ~activate ~deactivate ~id ?moment_title ?tempo
      ~part_title ~count the_part =
    let open Part in
    let count_string = match count with `Once -> "1" | `Infinity -> "∞" in
    (* dbg "Compiling %s × %s (title: %s) (tempo: %s)" count_string the_part.name
       part_title
       (Option.value_map ~f:(sprintf "%d bpm") tempo ~default:"NONE") ; *)
    let name =
      sprintf "[%d]P/%s/%s" (Main_track_id.as_int id)
        (Option.value moment_title ~default:"")
        the_part.name
    in
    (* dbg "Compile events: %s" the_part.name ; *)
    let part_track =
      let events = compile_part_events the_part.events in
      Scene.Track.fresh name ~length:(length_ticks the_part.length) ~events
    in
    (* dbg "Compiled events: %s" the_part.name ; *)
    let main_track_id = Main_track_id.render id in
    (* dbg "%s: main trackid: %s" the_part.name main_track_id ; *)
    let prev_next_moments_handler =
      Option.value_map previous_moment_id ~default:[] ~f:(fun trid ->
          set_moment_switch_handler ~buttons:prev_moment_buttons trid)
      @ Option.value_map next_moment_id ~default:[] ~f:(fun trid ->
            set_moment_switch_handler ~buttons:next_moment_buttons trid)
    in
    (* dbg "%s: prev_next_moments_handler: %d" the_part.name
       (List.length prev_next_moments_handler) ; *)
    let more_handlers =
      List.concat_map handlers ~f:(fun mh -> compile_meta_handler mh)
    in
    (* dbg "%s: more_handlers: %d" the_part.name (List.length more_handlers) ; *)
    let main_length, main_events =
      let part_track_id = Scene.Track.id part_track in
      let activated =
        List.concat_map activate ~f:(track_on q_first)
        @ List.concat_map deactivate ~f:(track_off q_first)
      in
      match count with
      | `Infinity ->
          let next_id = Main_track_id.(next id |> render) in
          let name_tag =
            Option.value_map next_announcement ~f:snd ~default:next_id
          in
          ( the_part.length,
            track_on q_first part_track_id
            @ set_next_handler ~end_of:main_track_id ~name_tag
                [
                  Scene.Action.Track_off part_track_id;
                  Scene.Action.Track_off main_track_id;
                  Scene.Action.Track_on (next_id, 1);
                ]
            @ activated )
      | `Once ->
          let lgth = the_part.length in
          ( lgth,
            track_on q_first part_track_id
            @ track_off Q.(one + lgth - (2 * q_epsilon)) part_track_id
            @ track_on
                Q.(one + lgth - (2 * q_epsilon))
                Main_track_id.(next id |> render)
            @ track_off Q.(one + lgth - (1 * q_epsilon)) main_track_id
            @ activated )
    in
    (* dbg "%s: main-length: %s, main_events: %d" the_part.name
       (Q.to_string main_length) (List.length main_events) ; *)
    let main_track =
      let name =
        sprintf "M/%s/%s/%s" count_string
          (Option.value moment_title ~default:"")
          the_part.name
      in
      let events =
        Option.value_map ~default:[] moment_title ~f:Message.set_moment_title
        @ Message.set_part_title part_title
        @ Message.set_next_announcement q_first "_"
        @ (if Poly.equal count `Infinity then
           (* already in the button handler *) []
          else
            Option.value_map ~default:[] next_announcement
              ~f:(fun (q_minus, msg) ->
                try
                  Message.set_next_announcement
                    Q.(one + the_part.length - q_minus)
                    msg
                with e ->
                  dbg
                    "set_next_announcement raised: q-minus %s : %S (part \
                     length: %s = %0.3f)"
                    (Q.to_string q_minus) msg
                    (Q.to_string the_part.length)
                    (Q.to_float the_part.length);
                  raise e))
        @ Option.value_map ~default:[] tempo ~f:set_bpm
        @ main_events @ prev_next_moments_handler @ more_handlers
      in
      Scene.Track.make ~id:main_track_id name ~length:(length_ticks main_length)
        ~events
    in
    (* dbg "%s: done" the_part.name ; *)
    [ main_track; part_track ]

  let metronome_track_id = function
    | TS_3_4 -> "metronome_3_4"
    | TS_4_4 -> "metronome_4_4"
    | TS_8_4 -> "metronome_8_4"
    | TS_9_4 -> "metronome_9_4"
    | TS_5_4 -> "metronome_5_4"
    | TS_6_4 -> "metronome_6_4"
    | TS_10_4 -> "metronome_10_4"
    | TS_11_4 -> "metronome_11_4"
    | TS_14_4 -> "metronome_14_4"
    | TS_7_4 -> "metronome_7_4"
    | TS_12_8 -> "metronome_12_8"
    | TS_33_8 -> "metronome_33_8"
    | TS_36_8 -> "metronome_36_8"

  let metronome_track tsig =
    (* printf "Creating %S\n%!" (metronome_track_id tsig) ; *)
    let _time_signature, name, bar_structure, length =
      match tsig with
      | TS_3_4 -> ((3, 4), "3/4", [ `Strong; `Strong; `Strong ], 3)
      | TS_4_4 -> ((4, 4), "4/4", [ `Strong; `Strong; `Strong; `Strong ], 4)
      | TS_8_4 ->
          ( (8, 4),
            "8/4",
            [ `Strong; `Weak; `Strong; `Weak; `Strong; `Weak; `Strong; `Weak ],
            8 )
      | TS_9_4 -> ((9, 4), "9/4", List.init 9 ~f:(fun _ -> `Strong), 9)
      | TS_5_4 ->
          ((5, 4), "5/4", [ `Strong; `Strong; `Strong; `Strong; `Strong ], 5)
      | TS_6_4 -> ((6, 4), "6/4", List.init 6 ~f:(fun _ -> `Strong), 6)
      | TS_10_4 ->
          ( (10, 4),
            "10/4",
            [
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
            ],
            10 )
      | TS_11_4 ->
          ( (11, 4),
            "11/4",
            [
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
            ],
            11 )
      | TS_14_4 ->
          ( (14, 4),
            "14/4",
            [
              `Strong;
              `Strong;
              `Weak;
              `Strong;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
              `Strong;
              `Weak;
            ],
            14 )
      | TS_7_4 ->
          ( (7, 4),
            "7/4",
            [ `Strong; `Strong; `Strong; `Strong; `Strong; `Strong; `Strong ],
            7 )
      | TS_12_8 -> ((4, 4), "12/8", [ `Strong; `Strong; `Strong; `Strong ], 4)
      | TS_33_8 -> ((11, 4), "33/8", List.init 11 ~f:(fun _ -> `Strong), 11)
      | TS_36_8 -> ((12, 4), "36/8", List.init 12 ~f:(fun _ -> `Strong), 12)
    in
    let events =
      []
      @ Option.value_map ~default:[] metronome_with_sound ~f:(fun `On_click ->
            let click note tick =
              let tpos =
                try position_ticks tick
                with e ->
                  Printf.eprintf "Click: (note: %d, tick: %s)\n%!" note
                    (Q.to_string tick);
                  raise e
              in
              let tlen = length_ticks Q.(1 / 16) in
              let port, channel = find_instrument instruments_mapping `Click in
              note_on_off ~port ~velocity:127 ~channel ~pitch:note ~tick:tpos
                ~length:tlen
            in
            List.init length ~f:(function
              | 0 -> click (click_note_one ()) Q.(one)
              | n -> click (click_note_other ()) Q.(one + i n))
            |> List.concat)
      @ Vidimetro.set_bar_structure Q.(i 1) ~length ~bar_structure
      @ Vidimetro.beats_and_progress ~length
    in
    Scene.Track.make name ~events
      ~length:(length_ticks Q.(i length))
      ~id:(metronome_track_id tsig)

  let compile_chunks chunks =
    let current_tempo = ref None in
    let current_handlers = ref [] in
    let current_title = ref None in
    let current_id = ref Main_track_id.first in
    let current_time_signature = ref TS_10_4 in
    let new_time_signature = ref TS_4_4 in
    let previous_moment_track = ref None in
    let next_moment_id = ref None in
    let next_moment_title t =
      let rec find = function
        | [] -> None
        | Moment { title; _ } :: _ -> Some title
        | List l :: t -> find (l @ t)
        | _ :: t -> find t
      in
      find t
    in
    let ts_ids = ref [] in
    let metronome_id_length ts =
      let metro_id = metronome_track_id ts in
      let lgth =
        match List.Assoc.find !ts_ids metro_id ~equal:String.equal with
        | None ->
            let m = metronome_track ts in
            ts_ids := (Scene.Track.id m, m) :: !ts_ids;
            (* printf "## m: %s, metro_id: %s\n%!" (Scene.Track.id m) metro_id ; *)
            m.Scene.Track.length
        | Some m -> m.Scene.Track.length
      in
      (metro_id, lgth)
    in
    let first_moment = ref None in
    let rec go_chunks all_tracks = function
      | [] -> all_tracks
      | c :: t ->
          let more_tracks = ref [] in
          let next = ref t in
          (match c with
          | Set_tempo c -> current_tempo := Some c
          | Set_handlers hl -> current_handlers := hl
          | Moment { count_in_tempo; title } ->
              let name = sprintf "Count in %s" title in
              let trid = sprintf "moment-%s" title in
              if Poly.equal !first_moment None then first_moment := Some trid;
              next_moment_id :=
                next_moment_title t |> Option.map ~f:(sprintf "moment-%s");
              let metro_id, metro_lgth =
                metronome_id_length !new_time_signature
              in
              let prev_metro_id, _ =
                metronome_id_length !current_time_signature
              in
              let events =
                Option.value_map !previous_moment_track ~default:[]
                  ~f:(fun trid ->
                    set_moment_switch_handler ~buttons:prev_moment_buttons trid)
                @ Option.value_map !next_moment_id ~default:[] ~f:(fun trid ->
                      set_moment_switch_handler ~buttons:next_moment_buttons
                        trid)
                @ set_next_handler ~name_tag:title ~end_of:trid
                    [
                      Scene.Action.Track_off trid;
                      Scene.Action.Track_on
                        (Main_track_id.(!current_id |> render), 1);
                    ]
                @ set_bpm count_in_tempo
                @ ksprintf Message.set_moment_title "Ready for: %s" title
                @ Message.set_part_title "_"
                @ Message.set_next_announcement q_first "_"
                @ Message.set_announcement q_first "_"
                @ Message.set_comment q_first " "
                @ track_on q_first metro_id
                @
                if String.(metro_id <> prev_metro_id) then
                  track_off q_first prev_metro_id
                else []
              in
              (* current_time_signature := count_in_ts; *)
              more_tracks :=
                [ Scene.Track.(make ~id:trid ~length:metro_lgth name ~events) ];
              previous_moment_track := Some trid;
              current_title := Some title
          | Time_signature ts -> new_time_signature := ts
          | Part_full { part_title; next; multiply; part } ->
              let activate =
                [ metronome_id_length !new_time_signature |> fst ]
              in
              let deactivate =
                if Poly.(!new_time_signature <> !current_time_signature) then
                  [ metronome_id_length !current_time_signature |> fst ]
                else []
              in
              current_time_signature := !new_time_signature;
              more_tracks :=
                compile_part ~count:multiply part ~id:!current_id
                  ?previous_moment_id:!previous_moment_track
                  ?next_moment_id:!next_moment_id ~activate ~deactivate
                  ?tempo:!current_tempo ?moment_title:!current_title
                  ?next_announcement:next ~handlers:!current_handlers
                  ~part_title;
              current_id := Main_track_id.next !current_id;
              current_title := None;
              current_tempo := None
          | List l -> next := l @ !next);
          go_chunks (all_tracks @ !more_tracks) !next
    in
    let main = go_chunks [] chunks in
    ( `Active [ !first_moment |> Option.value_exn ~message:"first_moment" ],
      `Tracks
        (Scene.Track.make
           ~id:Main_track_id.(render !current_id)
           "Last empty track"
           ~length:(length_ticks Q.(i 4))
           ~events:(Message.set_moment_title "This is the End…")
         :: main
        @ List.map !ts_ids ~f:snd) )

  let compile mix =
    let `Active active, `Tracks chunk_tracks = compile_chunks mix in
    let handlers =
      [
        Scene.Action.handler
          (handler_name incr_bpm_buttons "BPM + 5")
          ~events:(List.map incr_bpm_buttons ~f:Button.event)
          ~actions:(bpm_operation_actions (`Incr 5));
        Scene.Action.handler
          (handler_name decr_bpm_buttons "BPM - 5")
          ~events:(List.map decr_bpm_buttons ~f:Button.event)
          ~actions:(bpm_operation_actions (`Decr 5));
      ]
    in
    Scene.make ~handlers ~active ~bpm ~ppqn chunk_tracks
end
