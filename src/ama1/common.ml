include Base
module Format = Caml.Format
module Printf = Caml.Printf

let time_zero = Unix.gettimeofday ()

let dbg fmt =
  Format.ksprintf
    (fun s ->
      Printf.eprintf "[µ⬏ %.3f] %s\n%!" (Unix.gettimeofday () -. time_zero) s)
    fmt

module Q = struct
  type t =
    | Fraction of int * int
    | Plus of t * t
    | Times of int * t
    | Minus of t
  [@@deriving sexp]

  let float = Float.of_int
  let q a b = Fraction (a, b)
  let ( / ) a b = Fraction (a, b)
  let i a = Fraction (a, 1)
  let zero = i 0
  let one = i 1
  let ( + ) a b = Plus (a, b)
  let ( * ) a b = Times (a, b)
  let minus x = Minus x
  let ( - ) a b = Plus (a, minus b)
  let half = Fraction (1, 2)
  let third = Fraction (1, 3)
  let quarter = Fraction (1, 4)

  let rec to_float = function
    | Fraction (n, d) -> float n /. float d
    | Plus (a, b) -> to_float a +. to_float b
    | Times (a, b) -> float a *. to_float b
    | Minus x -> ~-.(to_float x)

  let floor f = to_float f |> Int.of_float

  let rec to_ticks ~ppqn =
    let open Stdlib in
    function
    | Fraction (n, d) -> n * ppqn / d
    | Plus (a, b) -> to_ticks ~ppqn a + to_ticks ~ppqn b
    | Times (a, b) -> a * to_ticks ~ppqn b
    | Minus x -> -to_ticks ~ppqn x

  let rec to_string =
    let open Printf in
    function
    | Fraction (i, 1) -> sprintf "%d" i
    | Fraction (i, n) -> sprintf "(%d/%d)" i n
    | Plus (q, r) -> sprintf "(%s + %s)" (to_string q) (to_string r)
    | Times (a, b) -> sprintf "(%d × %s)" a (to_string b)
    | Minus x -> sprintf "(- %s)" (to_string x)

  let compare a b =
    let af = to_float a in
    let bf = to_float b in
    Float.compare af bf

  let ( < ) a b = compare a b < 0
  let ( > ) a b = compare a b > 0
  let ( <= ) a b = compare a b <= 0
  let ( >= ) a b = compare a b >= 0

  let max a b =
    let af = to_float a in
    let bf = to_float b in
    if Float.(af < bf) then b else a
end

module Tempo = struct
  let linear_ramp ~start ~stop ~length =
    let open Float in
    let start = of_int start in
    let stop = of_int stop in
    let n_1 = of_int length -. 1. in
    List.init length ~f:(fun i ->
        (start * ((n_1 - of_int i) / n_1)) + (stop * of_int i / n_1)
        |> Int.of_float)
end
